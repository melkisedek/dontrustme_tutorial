#include <stdio.h>

int main(int argc, char* argv[]){
    if(argc < 2){
        fprintf(stderr, "Syntax: %s [STRING]\n",
                argv[0]);
    } else {
        printf("%s\n", argv[1]);
    }
}