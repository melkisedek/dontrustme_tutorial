#include <stdio.h>

int main(void){
    char string[21];
    
    gets(string); // this is not safe
    // use this instead fgets(string, 20, stdin);
    printf("string is: %s\n", string);
}