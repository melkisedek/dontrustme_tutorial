#include <stdio.h>

int main(int argc, char* argv[]){
    int n = 0;
    
    printf("The number of characters "
            "printed until this point X%n is "
            "stored in n\n", &n);
    printf("n is: %d\n", n);
}