#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h> //execve
#include <sys/stat.h> //chmod

/*
* this is the key we will use to XOR
*/
//#define XOR_KEY 0x6F

/*
* here is our extended multi-key XOR array
*/
unsigned char key[] = {0x03, 0x12, 0x4d, 0xe3, 0x11, 0x6f};

/*
* definitions for encryption or decryption
*/
#define JOB_CRYPT 1
#define JOB_DECRYPT 2

int xorFile(char *infile, char *outFile){
    FILE *fp, *newfile;
    int c = 0;
    
    fp = fopen(infile, "rb");
    newfile = fopen(outFile, "wb");
    
    //exit on open failure
    if(fp == NULL || newfile == NULL){
        perror("Failed to Open Files");
		fclose(fp);
		fclose(newfile);
		exit(1);
    }
    
    // XOR file contents with defined single byte XOR_KEY key
    // c = fgetc(fp);
    // while(c != EOF){
    //     fputc(c^XOR_KEY, newfile);
    //     c = fgetc(fp);
    // }
    
    /*
    * this is a multi-key method using multi-bytes to XOR the file contents
    */
    int i;
    for(i=0; c != EOF; i++){
        c = fgetc(fp);
        fputc(c^key[i % sizeof(key)], newfile);
    }
    
    //close streams
    fclose(fp);
    fclose(newfile);
    return 0;
}

int main(int argc, char *argv[]){
    if(argc <= 3){
        fprintf(stderr, 
            "Usage: %s [ENCRYPT | DECRYPT] [IN FILE ] [OUT FILE]\n", argv[0]);
        exit(1);
    }
    
    int job;
    if(strcmp(argv[1], "encrypt") == 0){
        job = JOB_CRYPT;
    } else if(strcmp(argv[1], "decrypt") == 0){
        job = JOB_DECRYPT;
    } else{
        fprintf(stderr, "Please select a suitable job\n");
        exit(1);
    }
    
    xorFile(argv[2], argv[3]);
    
    if(job == JOB_DECRYPT){
        char *args[] = {argv[3], NULL};
        
        int err = chmod(argv[3], S_IXUSR);
        if(err == -1){
            perror("Change mode");
            exit(1);
        }
        
        err = execve(args[0], args, NULL);
        if(err == -1){
            perror("Execute file");
            exit(1);
        }
    }
    
    return 0;
}