#include <stdio.h>

int main(void){
    int num;
    char string[21];
    
    puts("Please enter a number:");
    scanf("%d", &num);
    
    puts("Please enter a string "
            "no longer than 20 characters:");
    scanf("%s", string);
    
    printf("num id: %d\n", num);
    printf("string is: %s\n", string);
}