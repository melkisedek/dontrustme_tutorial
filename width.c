#include <stdio.h>

int main(int argc, char* argv[]){
    printf("Without width modifying: ");
    printf("%c\n", 'A');
    
    printf("Modifying with 10 minimum bytes: ");
    printf("%10c\n", 'A');
}