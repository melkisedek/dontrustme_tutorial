#include <stdio.h>
#include <stdlib.h>

typedef struct _person {
    char* firstname;
    char* surname;
    char gender;
    int age;
} Person, *pPerson;

pPerson newPerson(char* firstname, char* surname, char gender, int age){
    pPerson p = malloc(sizeof(struct _person));
    
    p->firstname = firstname;
    p->surname = surname;
    p->gender = gender;
    p->age = age;
    
    return p;
}

int main(int argc, char *argv[]){
    pPerson Moe = newPerson("Moe", "Lester", 'M', 68);
    
    printf("Name:\t%s %s\nGender:\t%c\nAge:\t%d\n",
            Moe->firstname, Moe->surname, Moe->gender, Moe->age);
            
    free(Moe);
}