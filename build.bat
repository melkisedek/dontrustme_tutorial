@echo off
call "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat" x64     
set compilerflags=/Od /Zi /TC /EHsc
set src="%~n1".c 
set out="%~n1".exe 

IF "%1"=="" (
    set linkerflags = /OUT:FileExample.exe
    cl.exe %compilerflags% FileExample.c /link %linkerflags%
) ELSE (
    set linkerflags=/OUT:%out%
    cl.exe %compilerflags% %src% /link %linkerflags%
)
    