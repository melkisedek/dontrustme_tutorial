#include <stdio.h>

int factorial(int n){
    //base case
    if(n == 0){
        return 1;
    }
    //call function again
    return n * factorial(n-1);
}

int main(int argc, char* argv[]){
    int num = 4;
    
    printf("Factorial of %d is: %d\n", num, factorial(num));
    
}