#include <stdio.h>

int main(int argc, char* argv[]){
    FILE* fp;
    /*
    char buf[1024];
    
    fp = fopen("myfile", "r");
    
    fgets(buf, sizeof(buf), fp);
    printf("File contents: %s\n", buf);
    */
    
    char buf[] = "The answer to the ultimate"
                " question of life is 42";
    
    fp = fopen("anotherfile", "w");
    
    fprintf(fp, "%s", buf);
    
    fclose(fp);
}