#include <stdio.h>

void swap(int *first, int *second){
    int temp;
    temp = *second;
    *second = *first;
    *first = temp;
}

int main(int argc, char *argv[]){
    int num1 = 42;
    int num2 = 68;
    
    swap(&num1, &num2);
    
    printf("num1 is: %d\nnum2 is: %d\n", num1, num2);
}