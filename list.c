#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef int Item;

typedef struct _node * link;

struct _node {
    Item item;
    link next;
};

typedef struct _list {
    link first;
} *list;

list newList(void){
    list l = malloc(sizeof(*l));
    assert(l != NULL);
    
    l->first = NULL;
    
    return l;
}

link newNode(Item i){
    link n = malloc(sizeof(*n));
    assert(n != NULL);
    
    n->item = i;
    n->next = NULL;
    
    return n;
}

void freeList(list l){
    /*
    * temp is a pointer to a struct _node or simply a node
    */
    link temp;
    
    /*
    * we use temp like an iterator and process the entire list starting from
    * the first node and finishing when we reach a NULL
    */
    for(temp = l->first; temp != NULL; temp = l->first){
        /*
        * move the node-to-be-freed's next node to become the first node 
        * i.e. move second node to first position and then free 
        * the previous first node etc
        */
        l->first = temp->next;
        
        /*
        * free the node pointed to by temp
        */
        free(temp);

    }
    // free head
    free(l);
}

void appendNode(list l, link n){
    /*
    * check if list is empty and add the node as first if list is empty
    */
    if(l->first == NULL){
        /*
        * set first node as the node to be appended
        */
        l->first = n;
    } else {
        /*
        * make a node pointer curr(ent) to point to the node we are currently at
        */
        link curr;
        
        /*
        * traverse list until we reach the last node which has its 
        * "next" pointer pointing to NULL
        */
        for(curr = l->first; curr->next != NULL; curr = curr->next);
        
        /*
         * set last node's "next" equal to the node to be appended 
        */
        curr->next = n;
    }
}

void printList(list l){
    link temp;
    for(temp = l->first; temp != NULL; temp = l->first){
        printf("[HEAD @ %p | First: %p]\n", l, l->first);
        printf("[NODE @ %p | Item: %d | Next: %p]\n", 
            temp, temp->item, temp->next);
        l->first = temp->next;
    }
}

int main(int argc, char *argv[]){
    
    if(argc < 2){
        fprintf(stderr, "No items specified\n");
        exit(EXIT_FAILURE);
    }
    
    // Initialise list
    list l = newList();
    
   /*
   * parse command line arguments and create a new node for each number
   * then append it to the list
   */
    int i;
    for(i = 1; i < argc; i++){
        appendNode(l, newNode(atoi(argv[i])));
    }
   
   /*
   * my implementation to print the list
   */
   printList(l);
   
   //free the last after finishing
   freeList(l);
    return EXIT_SUCCESS;
}