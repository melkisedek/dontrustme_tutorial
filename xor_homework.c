#include <stdio.h>

#define XOR_BYTE 0xEF

void xorFile(FILE *infile, FILE *outfile){
    int c;
    c = fgetc(infile);
    while(c != EOF){
        c ^= XOR_BYTE;
        fputc(c, outfile);
        c = fgetc(infile);
    }
}

int main(int argc, char *argv[]){
    FILE *infile, *outfile;
    infile = fopen(argv[1], "rb");
    outfile = fopen(argv[2], "wb");
    
    if(infile == NULL || outfile == NULL){
        perror("Failed to Open Files");
		fclose(infile);
		fclose(outfile);
    }
    
    xorFile(infile, outfile);
    fclose(infile);
    fclose(outfile);
    
    return 0;
}