// Personal exercise on how to use linked lists
#include <stdio.h>
struct Node{
	int num;
	struct Node * next;
};


int main(){
	struct Node one;
	struct Node two;
	struct Node three;
	
	one.num = 1;
	two.num = 2;
	three.num = 3;
	one.next = &two;
	two.next = &three;
	three.next = NULL;
	struct Node *list;
	list = &one;
	for ( ; list != NULL; list = list->next)
        	printf("%i\n", (*list).num);
	return 0;
}
