#include <stdio.h>
#include <string.h>
#include <Windows.h>

int main(int argc, char *argv[]){
    if(argc < 3){
        fprintf(stderr, "Usage: %s <file name> <text>\n", argv[0]);
        return 1;
    }
    
    HANDLE hFile = CreateFile(argv[1], GENERIC_READ | GENERIC_WRITE,
                            FILE_SHARE_READ, NULL, CREATE_ALWAYS,
                            FILE_ATTRIBUTE_NORMAL, NULL);
    if(hFile == INVALID_HANDLE_VALUE){
        fprintf(stderr, "Create file error: %lu\n", GetLastError());
        return 1;
    }
    
    DWORD dwWritten = 0;
    DWORD dwErr = WriteFile(hFile, argv[2], strlen(argv[2]), &dwWritten, NULL);
    if(dwErr == FALSE){
        CloseHandle(hFile);
        fprintf(stderr, "Write file error: %lu\n", GetLastError());
        return 1;
    }
    
    printf("Wrote %lu bytes to %s\n", dwWritten, argv[1]);
    
    SetFilePointer(hFile, 0, NULL, FILE_BEGIN);
    
    DWORD dwRead = 0;
    CHAR ReadBuf[BUFSIZ];
    dwErr = ReadFile(hFile, ReadBuf, BUFSIZ-1, &dwRead, NULL);
    if(dwErr == FALSE){
        CloseHandle(hFile);
        fprintf(stderr, "Read file error: %lu\n", GetLastError());
        return 1;
    }
    
    ReadBuf[dwRead] = '\0';
    
    printf("Read %lu bytes:\n%s\n", dwRead, ReadBuf);
    
    CloseHandle(hFile);
    
    return 0;
}