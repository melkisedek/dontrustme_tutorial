#include <stdio.h>

int add(int a, int b){
    return a + b;
}

int main(void){
    int sum;
    int num1 = 12;
    int num2 = 30;
    
    sum = add(num1, num2);
    
    printf("The sum of %d and %d is: %d\n", num1, num2, sum);
}