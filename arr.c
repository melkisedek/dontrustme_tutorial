#include <stdio.h>

void editArray(int *arr, int size){
    int i;
    for(i = 0; i < size; i++){
        arr[i] *= 2;
    }
}

int main(void){
    int array[] = {1, 2, 3, 4, 5};
    int size = sizeof(array)/sizeof(int);
    
    editArray(array, size);
    
    int i;
    for(i=0; i < size; i++){
        printf("Array[%d] = %d\n", i, array[i]);
    }
}